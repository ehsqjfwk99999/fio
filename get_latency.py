import os


def main():
    results = []

    print("\n################## Average latency ##################\n")

    test_modes = ["vanilla", "aios"]
    block_sizes = ["4K", "8K", "16K", "32K", "64K", "128K"]
    thread_nums = [1, 2, 4, 8, 16, 32]

    for test_mode in test_modes:
        for block_size in block_sizes:
            for thread_num in thread_nums:
                file_name = f"{test_mode}_{block_size}_{thread_num}"
                file_path = os.path.join("./logs", file_name)
                if os.path.isfile(file_path):
                    avg_latency = get_latency(file_path)
                    result_str = f"{'Vanilla' if test_mode == 'vanilla' else 'AIOS'} test with {block_size} block size, {thread_num} threads : {avg_latency:.2f}"
                    print(result_str)
            print()
        print()


def get_latency(file_path):
    total_latency = 0
    count = 0
    with open(file_path) as f:
        while True:
            line = f.readline()
            if not line:
                break
            if line.startswith("    sync ("):
                tokens = line.split()
                scale = tokens[1].strip("():")
                latency = tokens[4].strip("avg=,")
                if scale.startswith("m"):
                    latency = 1000 * float(latency)
                elif scale.startswith("u"):
                    latency = float(latency)
                else:
                    raise ValueError("Not usec or msec")
                total_latency += latency
                count += 1
    avg_latency = total_latency / count
    return avg_latency


if __name__ == "__main__":
    main()