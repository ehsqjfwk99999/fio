#!/bin/bash

########################### Test Options ###########################

# Test Options
do_vanilla=0
do_aios=1
do_block_size=1
 bs_list=(4K 8K 16K 32K 64K 128K)
# bs_list=(4K)
do_thread=0
# numjobs_list=(1 2 4 8 16 32)
# numjobs_list=(2)
iteration=100

# Global Fio Options
directory=/home/ksy/btrfs_dir
filename=write_test
fadvise_hint=1
thread=1
group_reporting=1
ioengine=sync
fsync=1
rw=randwrite
iodepth=1
name=test
size=1M

########################### Test Setup ###########################

# Init log Files.
rm -r ./logs
mkdir logs

# Create gloabl option string.
global_option_string="
--directory=${directory}
--filename=${filename}
--fadvise_hint=${fadvise_hint}
--thread=${thread}
--group_reporting=${group_reporting}
--ioengine=${ioengine}
--fsync=${fsync}
--rw=${rw}
--iodepth=${iodepth}
--name=${name}
--size=${size}
"

# Check test mode.
fio_modes=()
if [ ${do_vanilla} -eq 1 ]; then
    fio_modes+=("vanilla")
fi
if [ ${do_aios} -eq 1 ]; then
    fio_modes+=("aios")
fi

# Check block size test.
#if [ ${do_block_size} -eq 1 ]; then
#    bs_list=(4K 8K 16K 32K 64K 128K)
#fi
# Check thread test.
#if [ ${do_thread} -eq 1 ]; then
#    numjobs_list=(1 2 4 8 16 32)
#fi

########################### Testing ###########################

# Test Loop
echo
for fio_mode in ${fio_modes[*]}; do
    if [ ${do_block_size} -eq 1 ]; then
        # Block Size Test
        numjobs=1
        for bs in ${bs_list[*]}; do
            option_string="${global_option_string} --bs=${bs} --numjobs=${numjobs}"
            log_path=./logs/${fio_mode}_${bs}_${numjobs}
            touch ${log_path}
            echo -n "${fio_mode} testing with block size=${bs}, thread=${numjobs} ... "
            for ((i=1;i<=${iteration};i++)); do
                ./init/drop.sh > /dev/null
                ./init/prepare_clock.sh > /dev/null
                echo -n "${i} "
                ./bin/fio_${fio_mode} ${option_string} >> ${log_path}
                echo -ne "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" >> ${log_path}
                echo -e ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" >> ${log_path}
            done
            echo
        done
        echo
    fi

    if [ ${do_thread} -eq 1 ]; then
        # Thread Test
        bs=4K
        for numjobs in ${numjobs_list[*]}; do
            option_string="${global_option_string} --bs=${bs} --numjobs=${numjobs}"
            log_path=./logs/${fio_mode}_${bs}_${numjobs}
            touch ${log_path}
            echo -n "${fio_mode} testing with block size=${bs}, thread=${numjobs} ... "
            for ((i=1;i<=${iteration};i++)); do
                ./init/drop.sh > /dev/null
                ./init/prepare_clock.sh > /dev/null
                echo -n "${i} "
                ./bin/fio_${fio_mode} ${option_string} >> ${log_path}
                echo -ne "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" >> ${log_path}
                echo -e ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" >> ${log_path}
            done
            echo
        done
        echo
    fi
done
echo
