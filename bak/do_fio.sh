#!/bin/bash

########################### Test Options ###########################

# Test Options
do_vanilla=1
do_aios=0
do_block_size=1
do_thread=1
iter=3

# Global Fio Options
directory=/mnt/btrfs_dir
filename=write_test
fadvise_hint=1
thread=1
group_reporting=1
ioengine=sync
fsync=1
rw=randwrite
iodepth=1
name=test
size=1M

########################### Test Setup ###########################

# Init log Files.
rm -r ./logs
mkdir logs

# Create gloabl option string.
global_option_string="
--directory=${directory}
--filename=${filename}
--fadvise_hint=${fadvise_hint}
--thread=${thread}
--group_reporting=${group_reporting}
--ioengine=${ioengine}
--fsync=${fsync}
--rw=${rw}
--iodepth=${iodepth}
--name=${name}
--size=${size}
"

function do_fio() {
    # Check test mode.
    if [ ${1} == V ]; then
        fio_title=Vanilla
        fio_file=vanilla
    else
        fio_title=AIOS
        fio_file=aios
    fi
    # Check block size test.
    if [ ${2} -eq 1 ]; then
        bs_list=(4K 8K 16K 32K 64K 128K)
    else
        bs_list=(4K)
    fi
    # Check thread test.
    if [ ${3} -eq 1 ]; then
        numjobs_list=(1 2 4 8 16 32)
    else
        numjobs_list=(1)
    fi
    
    # Test Loop
    for numjobs in ${numjobs_list[*]}; do
        for bs in ${bs_list[*]}; do
            option_string="${global_option_string} --bs=${bs} --numjobs=${numjobs}"
            log_path=./logs/${fio_file}_${bs}_${numjobs}
            touch ${log_path}
            echo -n "${fio_title} testing with block size=${bs}, thread=${numjobs} ... "
            for ((i=1;i<=${iter};i++)); do
                ./init/drop.sh > /dev/null
                ./init/prepare_clock.sh > /dev/null
                echo -n "${i} "
                ./bin/fio_${fio_file} ${option_string} >> ${log_path}
                echo -ne "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" >> ${log_path}
                echo -e ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" >> ${log_path}
            done
            echo
        done
        echo
    done
}

echo

########################### Vanilla Test ###########################
if [ ${do_vanilla} -eq 1 ]; then
    fio_mode=V
    do_fio ${fio_mode} ${do_block_size} ${do_thread}
fi

########################### AIOS Test ###########################
if [ ${do_aios} -eq 1 ]; then
    fio_mode=A
    do_fio ${fio_mode} ${do_block_size} ${do_thread}
fi
