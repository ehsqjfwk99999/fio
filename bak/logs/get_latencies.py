
f = open('aios_result.txt', 'r')
ff = open('aios_latencies.txt', 'w')

total_latency = 0
count = 0
while True:
    line = f.readline()
    if not line:
        break
    
    line = line.split()
    if len(line)>1 and line[0]=='sync' and line[1]=='(usec):':
            latency = float(line[4].strip('avg=,'))
            total_latency += latency
            count += 1
            ff.write(f'#{count} : {latency}\n')

print('##### AIOS Result #####')
print(f'=> iter times : {count}')
print(f'=> average latency : {total_latency/count}')

ff.write('\n')
ff.write(f'=> iter times : {count}\n')
ff.write(f'=> average latency : {total_latency/count}\n')


f.close()
ff.close()

fv = open('vanilla_result.txt', 'r')
ffv = open('vanilla_latencies.txt', 'w')

total_latency = 0
count = 0
while True:
    line = fv.readline()
    if not line:
        break

    line = line.split()
    if len(line)>1 and line[0]=='sync' and line[1]=='(usec):':
            latency = float(line[4].strip('avg=,'))
            total_latency += latency
            count += 1
            ffv.write(f'#{count} : {latency}\n')

print('##### Vanilla Result #####')
print(f'=> iter times : {count}')
print(f'=> average latency : {total_latency/count}')

ffv.write('\n')
ffv.write(f'=> iter times : {count}\n')
ffv.write(f'=> average latency : {total_latency/count}\n')


fv.close()
ffv.close()
