#!/usr/bin/env python3

import subprocess

########################### Test Options ###########################

# Test Options
do_vanilla = 1
do_aios = 0
do_block_size = 0
do_thread = 0
iteration = 3

########################### Test Setup ###########################

# Check test mode.
test_modes = []
if do_vanilla:
    test_modes.append("vanilla")
if do_aios:
    test_modes.append("aios")

# Check block size test.
block_sizes = []
if do_block_size:
    block_sizes = ["4K", "8K", "16K", "32K", "64K", "128K"]
else:
    block_sizes.append("4K")

# Check thread test.
thread_nums = []
if do_thread:
    thread_nums = [1, 2, 4, 8, 16, 32]
else:
    thread_nums.append(1)

# Result Array
results = []

########################### Testing ###########################

# Test Loop
print()
for test_mode in test_modes:
    for block_size in block_sizes:
        for thread_num in thread_nums:
            print(
                f"Testing {test_mode} with {block_size} block size, {thread_num} threads => ",
                end="",
            )
            total_latency = 0
            for i in range(iteration):
                print(i + 1, end=" ", flush=True)
                command = f"./execute_fio.sh {test_mode} {block_size} {thread_num}"
                fio_process = subprocess.Popen(
                    command,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    shell=True,
                )
                fio_result, _ = fio_process.communicate()
                fio_result = fio_result.decode()

                for line in fio_result.split("\n"):
                    if line.strip().startswith("sync (msec)"):
                        latency = line.split()[4].strip("avg=,")
                        total_latency += float(latency)
                        break

            avg_latency = total_latency / iteration
            result_str = f"{'Vanilla' if test_mode == 'vanilla' else 'AIOS'} test with {block_size} block size, {thread_num} threads : {avg_latency:.2f}"
            results.append(result_str)
            print()
    print()

# Print result.
print("\n################ Average latency ################")
for result in results:
    print(result)
print()