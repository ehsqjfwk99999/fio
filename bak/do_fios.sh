#! /bin/bash

echo -e "\n===> Flushing result files ..."
echo > ./logs/aios_result.txt
echo > ./logs/vanilla_result.txt
echo -e "===> Done\n"

echo -e "===> Starting vanilla ..."
i=0
while [ $i -lt 10 ]; do
    ./vanilla >> ./logs/vanilla_result.txt
	echo vanilla#$(($i+1))
    i=$(($i+1))
done
echo -e "===> Done\n"

echo -e "===> Starting aios ..."
i=0
while [ $i -lt 10 ]; do
    ./aios >> ./logs/aios_result.txt
    echo aios#$(($i+1))
    i=$(($i+1))
done
echo -e "===> Done\n"

echo -e "===> Extracting latencies ..."
cd logs/
python3 get_latencies.py
cd ..
echo -e "===> Done\n"
