#!/bin/bash

########################### Test Options ###########################

# Global Fio Options
directory=/mnt/btrfs_dir
filename=write_test
fadvise_hint=1
thread=1
group_reporting=1
ioengine=sync
fsync=1
rw=randwrite
iodepth=1
name=test
size=1M

########################### Test Setup ###########################

# Input Arguments.
test_mode=$1
block_size=$2
thread_num=$3

# Create gloabl option string.
global_option_string="
--directory=${directory}
--filename=${filename}
--fadvise_hint=${fadvise_hint}
--thread=${thread}
--group_reporting=${group_reporting}
--ioengine=${ioengine}
--fsync=${fsync}
--rw=${rw}
--iodepth=${iodepth}
--name=${name}
--size=${size}
"

# Create complete option string.
option_string="${global_option_string} --bs=${block_size} --numjobs=${thread_num}"

# Create test file name.
test_file="fio_${test_mode}"

########################### Test ###########################

# Drop cache.
./init/drop.sh > /dev/null
# ?
./init/prepare_clock.sh > /dev/null
# Execute test.
./bin/${test_file} ${option_string}
