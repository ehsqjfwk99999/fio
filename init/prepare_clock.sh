#!/bin/bash

/etc/init.d/cpufrequtils stop

echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo

for i in $(seq 0 7)
do  
	#for j in $(seq 2 4)
	#do  
#	for ((j = 2; j < 5; j++)) do
	#	echo 1 > /sys/devices/system/cpu/cpu"$i"/cpuidle/state"$j"/disable
	#done
	#for j in $(seq 0 1)
	#do
#	for ((j = 0; j < 2; j++)) do
	#	echo 0 > /sys/devices/system/cpu/cpu"$i"/cpuidle/state"$j"/disable
	#done
	echo "performance" > /sys/devices/system/cpu/cpu"$i"/cpufreq/scaling_governor
#echo 3600000 > /sys/devices/system/cpu/cpu"$i"/cpufreq/scaling_max_freq
#echo 3600000 > /sys/devices/system/cpu/cpu"$i"/cpufreq/scaling_min_freq
	#echo 2600000 > /sys/devices/system/cpu/cpu"$i"/cpufreq/scaling_cur_freq
	cpufreq-set -c "$i" -d 2.60Ghz
    cat /sys/devices/system/cpu/cpu"$i"/cpufreq/scaling_cur_freq
done

echo 100 > /sys/devices/system/cpu/intel_pstate/max_perf_pct
echo 100 > /sys/devices/system/cpu/intel_pstate/min_perf_pct

#echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
#echo "You should perform echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo manually"
cat /sys/devices/system/cpu/intel_pstate/no_turbo

